import requests
import re
import yaml
from pprint import pprint

#api aanspreken 
api_base_url = "https://api.domainsdb.info/v1/domains/search?domain=syntra.be"

response=requests.get(f"{api_base_url}/broadcast_messages")

response_data=response.json()
# pprint(response_data)

#gevraagde gegevens uit json halen
ip = response_data['domains'][0]['A'][0]
land = response_data['domains'][0]['country']
created_date = response_data['domains'][0]['create_date']
provider = response_data['domains'][0]['NS'][0]

#regex voor de provider uit de opgesplitste json te halen
regex_provider = r"\.(?P<provider>\w+)\."
matches = re.findall(regex_provider, provider)

#regexen voor de gegevens uit de opgesplitste json te halen en terug weer tegeven in een dictionarry
regex_jaar = r"(?P<jaar>[0-9]{4})-"
regex_maand = r"-(?P<maand>[0-9]{2})-"
regex_dag = r"-(?P<dag>\d+)[A-Z]"

regex_list = [regex_jaar,regex_maand,regex_dag]
created={}
for regex in regex_list:
    for match in re.finditer(regex, created_date, flags=re.MULTILINE):
        created=created | match.groupdict()
        
#dict opbouwen met de gevraagde gegevens
result= {'ip': ip , 'land' : land, 'provider': matches[0], 'created' : created}

#dict naar yaml en weergeven 
print(yaml.dump(result))
